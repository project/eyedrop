jQuery.fn.eyedrop = function () {
  $.eyedrop(this);
  return this;
};

// Called to initialize and display layers
jQuery.eyedrop = function (container) {

  var container = $(container).get(0);
  return container.eyedrop || (container.eyedrop = new jQuery._eyedrop(container));
}

jQuery._eyedrop = function (container, callback) {

  // Store eyedrop object
  var eye = this;

  // Setting some parameters here, need more control over these later.
  var action = 'crop';
  var target = '';
  var group = container.id.split('-').pop();
  var canvasWidth = '500px';
  var handleOffset = 4; // Displacement of handle on crop boundary
  var imgCropMin = 10; // Minimum cropping size in pixels.

  // Get the canvas ratio (ratio of original image).
  // TODO: This line is not compatible with IE6 or IE7 (getting the image size).
  var ratioOrig = parseInt(jQuery("#eye-image-orig").get(0).width) / parseInt(jQuery("#eye-image-orig").get(0).height);
  var ratioSelect = 0; // 0 means no ratio applied.

  // drag[] is an array for capturing values during various mouse events.
  var drag = new Array(0,0,0,0,0,0,0,0,0,0,0,0);
  // limit[] is an array for setting the drag boundaries.
  var limit = new Array(0,0,0,0);

  // X and Y = current mouse position
  // X1 and Y1 = starting position.
  // L and T captures the initial position of a moving object
  // W and H captures the horizontal/vertical distance from X and Y.
  // CanvasW and CanvasH captures the size of the canvas on mousedown
  var X = 0; var Y = 1; var X1 = 2; var Y1 = 3; var X2 = 4; var Y2 = 5; var L = 6; var T = 7; var W = 8; var H = 9; var CanvasW = 10; var CanvasH = 11;

  var oCanvas = jQuery("#eye-image").get(0).style;
  // Apply canvas width;
  oCanvas.width = canvasWidth;

  // Find the ratio between the canvas size and the original image.
  // TODO: This line is not compatible with IE6 or IE7 (getting the image size).
  var zoom = parseInt(oCanvas.width) / jQuery("#eye-image-orig").get(0).width;

  var oSelect = new Object();
  var oSelect = jQuery("#eye-crop").get(0).style;

  // The masks are the four divs surrounding the crop area.
  var oMask = new Object();
  oMask.North = jQuery("#eye-mt").get(0).style;
  oMask.West = jQuery("#eye-ml").get(0).style;
  oMask.South = jQuery("#eye-mb").get(0).style;
  oMask.East = jQuery("#eye-mr").get(0).style;

  // The handles are the images in each corner that can be dragged to resize the crop area.
  var oHand = new Object();
  oHand.NE = jQuery("#eye-ne").get(0).style;
  oHand.NW = jQuery("#eye-nw").get(0).style;
  oHand.SE = jQuery("#eye-se").get(0).style;
  oHand.SW = jQuery("#eye-sw").get(0).style;

  // Build references to the value display input elements.
  var dsp = new Array();
  $("#eyedrop-parameters").find("input").each( function(i) {
    dsp[this.id.split('-').pop()] = this;
  });

  // Gather the parameters for this layer.
  var params = new Array();
  $(container).find(".eyedrop-group-param").each( function(i) {
    params[this.id.split('-').pop()] = this.value;
  });

  /**
   * Editor initialization
   */
  eye.initialize = function (action) {

    drag[L]  = (params['x'] * zoom);
    drag[W]  = (params['w'] * zoom);
    drag[T]  = (params['y'] * zoom);
    drag[H]  = (params['h'] * zoom);
    drag[CanvasW] = parseInt(oCanvas.width);
    drag[CanvasH] = (drag[CanvasW] / ratioOrig);

    oHand.SW.left = oHand.NW.left = oHand.NW.top = oHand.NE.top = "-"+handleOffset+'px';

    eye.maskResizeLeft(drag[L], drag[W], drag[CanvasW]);
    eye.maskResizeTop(drag[T], drag[H], drag[CanvasH]);
    eye.maskResizeRight(drag[L] + drag[W], drag[W], drag[CanvasW]);
    eye.maskResizeBottom(drag[T] + drag[H], drag[H], drag[CanvasH]);

    // This syntax to output values into display boxes
    dsp['debug1'].value = "Help to debug";
    dsp['debug2'].value = "is in code";
  }

  /**
   * Mousedown handler
   */
  eye.mousedown = function (event) {

    // Capture mouse
    if (!document.dragging) {
      $(document).bind('mousemove', eye.mousemove).bind('mouseup', eye.mouseup);
      document.dragging = true;

      // Record the initial mouse position.
      drag[X1] = event.clientX;
      drag[Y1] = event.clientY;
      drag[CanvasW] = parseInt(oCanvas.width);
      drag[CanvasH] = (drag[CanvasW] / ratioOrig);

      target = event.target.id || event.srcElement.id;

      drag[T] = parseInt(oSelect.top);
      drag[L] = parseInt(oSelect.left);
      drag[W] = parseInt(oSelect.width);
      drag[H] = parseInt(oSelect.height);

      eye.setMouseLimits(target);

      // Process this event like a mouse move.
      eye.mousemove(event);
    }
    return false;
  }

  /**
   * Mousemove handler
   */
  eye.mousemove = function (event) {

    // Get main coordinates
    drag[X] = event.clientX;
    drag[Y] = event.clientY;

    var xMove = (drag[X] - drag[X1]);
    var yMove = (drag[Y] - drag[Y1]);

    xMove = Math.max(limit[X1], Math.min (limit[X2], xMove));
    yMove = Math.max(limit[Y1], Math.min (limit[Y2], yMove));

    switch(target) {
      case 'eye-crop':
        eye.maskMoveX(drag[L] + xMove, drag[W], drag[CanvasW]);
        eye.maskMoveY(drag[T] + yMove, drag[H], drag[CanvasH]);
        break;
      case 'eye-nw':
        eye.maskResizeLeft(drag[L] + xMove, drag[W] - xMove, drag[CanvasW]);
        eye.maskResizeTop(drag[T] + yMove, drag[H] - yMove, drag[CanvasH]);
        break;
      case 'eye-ne':
        eye.maskResizeRight(drag[L] + drag[W] + xMove, drag[W] + xMove, drag[CanvasW]);
        eye.maskResizeTop(drag[T] + yMove, drag[H] - yMove, drag[CanvasH]);
        break;
      case 'eye-sw':
        eye.maskResizeLeft(drag[L] + xMove, drag[W] - xMove, drag[CanvasW]);
        eye.maskResizeBottom(drag[T] + drag[H] + yMove, drag[H] + yMove, drag[CanvasH]);
        break;
      case 'eye-se':
        eye.maskResizeRight(drag[L] + drag[W] + xMove, drag[W] + xMove, drag[CanvasW]);
        eye.maskResizeBottom(drag[T] + drag[H] + yMove, drag[H] + yMove, drag[CanvasH]);
        break;
    }
  }

  /**
  * Mouseup handler
  */
  eye.mouseup = function () {
    // Uncapture mouse
    $(document).unbind('mousemove', eye.mousemove);
    $(document).unbind('mouseup', eye.mouseup);
    document.dragging = false;
  }

  eye.maskMoveX = function (selectLeft, selectWidth, canvasWidth) {

    oSelect.left       = selectLeft + 'px';
    oMask.West.width   = selectLeft + 'px';
    oMask.North.left   = selectLeft + 'px';
    oMask.South.width  = selectLeft + selectWidth + "px";
    oMask.East.left    = selectLeft + selectWidth + "px";
    oMask.North.width  = canvasWidth - selectLeft + "px";
    oMask.East.width   = canvasWidth - selectLeft - selectWidth + 'px';
  }

  eye.maskMoveY = function (selectTop, selectHeight, canvasHeight) {
    oSelect.top        = selectTop + 'px';
    oMask.North.height = selectTop + 'px';
    oMask.East.top     = selectTop + 'px';
    oMask.West.height  = selectTop + selectHeight + "px";
    oMask.South.top    = selectTop + selectHeight + "px";
    oMask.East.height  = canvasHeight - selectTop + "px";
    oMask.South.height = canvasHeight - selectTop - selectHeight + 'px';
  }

  eye.maskResizeLeft = function (selectLeft, selectWidth, canvasWidth) {
    oSelect.left      = selectLeft + 'px';
    oSelect.width     = selectWidth + 'px';
    oMask.West.width  = selectLeft + 'px';
    oMask.North.left  = selectLeft + 'px';
    oMask.North.width = canvasWidth - selectLeft + "px";
    oHand.NE.left     = selectWidth - handleOffset + "px";
    oHand.SE.left     = selectWidth - handleOffset + "px";
  }

  eye.maskResizeRight  = function (selectRight, selectWidth, canvasWidth) {

    oSelect.width      = selectWidth + 'px';
    oMask.East.left    = selectRight + "px";
    oMask.South.width  = selectRight + "px";
    oMask.East.width   = canvasWidth - selectRight + "px";
    oHand.NE.left      = selectWidth - handleOffset + "px";
    oHand.SE.left      = selectWidth - handleOffset + "px";
  }

  eye.maskResizeTop = function (selectTop, selectHeight, canvasHeight) {
    oSelect.top        = selectTop + 'px';
    oSelect.height     = selectHeight + 'px';
    oMask.North.height = selectTop + "px";
    oMask.East.top     = selectTop + "px";
    oMask.East.height  = canvasHeight - selectTop + "px";
    oHand.SW.top       = selectHeight - handleOffset + "px";
    oHand.SE.top       = selectHeight - handleOffset + "px";
  }

  eye.maskResizeBottom = function (selectBottom, selectHeight, canvasHeight) {
    oSelect.height     = selectHeight + 'px';
    oMask.South.top    = selectBottom + "px";
    oMask.West.height  = selectBottom + "px";
    oMask.South.height = canvasHeight - selectBottom + "px";
    oHand.SW.top       = selectHeight - handleOffset + "px";
    oHand.SE.top       = selectHeight - handleOffset + "px";
  }

  eye.setMouseLimits = function (target) {

    limit[X1] = parseInt(oMask.West.width) * -1;
    limit[Y1] = parseInt(oMask.North.height) * -1;
    limit[X2] = parseInt(oMask.East.width);
    limit[Y2] = parseInt(oMask.South.height);
    var width = parseInt(oSelect.width) - imgCropMin;
    var height = parseInt(oSelect.height) - imgCropMin;

    switch (target) {
      case 'eye-nw':
        limit[X2] = width;
        limit[Y2] = height;
        break;
      case 'eye-ne':
        limit[X1] = width * -1;
        limit[Y2] = height;
        break;
      case 'eye-sw':
        limit[Y1] = height * -1;
        limit[X2] = width;
        break;
      case 'eye-se':
        limit[X1] = width * -1;
        limit[Y1] = height * -1;
        break;
    }
  }

  // This returns the true width of the crop, if the ratio of width:height
  // should be fixed.
  eye.fixedRatioWidth = function (kX, kY) {
    var a1 = (Math.atan(imgOrigHeight/imgOrigWidth) * (180 / Math.PI));
    var a2 = 90 - a1;
    var a3 = (Math.atan(kY/kX) * (180 / Math.PI))
    var a4 = a3 - a2;
    var k3 = Math.sqrt(Math.pow(kX,2)+Math.pow(kY,2));
    var k6 = Math.sin(a4 / (180 / Math.PI)) * k3;
    var finalx =  Math.cos(a1) * k6;
    return finalx;
  }

    // Show the editor.
  jQuery("#eyedrop-container").fadeIn();

  // Initialize
  eye.initialize('crop');

  // Install mousedown handler (the others are set on the document on-demand)
  $('*', "#eyedrop-editor").mousedown(eye.mousedown);

  // Set linked elements/callback
  if (callback) {
    eye.linkTo(callback);
  }
}